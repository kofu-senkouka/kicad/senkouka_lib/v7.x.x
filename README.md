# kicad『7』用のライブラリです

## kicad-senkouka-lib
本ライブラリは、授業で使用するKicadのライブラリになります。

## ライセンス
ライセンスはフリーとしますが、再配布は行わないようにしてください。<br>
また、個人的に使用しても良いですが、何があっても自己責任でお願いします。

## インストール方法
①GitHubにあるコードを任意の場所にダウンロードします。<br>
②ダウンロードしたフォルダにある『シンボルライブラリ』と『フットプリントライブラリ』をKicadのライブラリに登録します。<br>
③Kicadのライブラリへの登録は、『設定』の各ライブラリの管理から行います。<br>
<img src="https://gitlab.com/kofu-senkouka/kicad/senkouka-lib/-/raw/doc/244253903-d4bde8cf-3d6a-42ed-87f5-86a03c296f92.png" width="80%" /><br>
④ライブラリの管理画面を開いたら、下の方にある『フォルダを開く』を選択して、ダウンロードしたライブラリを選択します。<br>
<img src="https://gitlab.com/kofu-senkouka/kicad/senkouka-lib/-/raw/doc/244254623-b2f73264-187d-4f70-a276-5524acfe2e96.png" width="70%" /><br>
<img src="https://gitlab.com/kofu-senkouka/kicad/senkouka-lib/-/raw/doc/244253045-7d045f4f-fca8-445a-9da2-d19e67e246cc.png" width="70%" /><br>
⑤『senkouka_lib』がライブラリに登録されていればOKです。<br>
　※『フットプリントライブラリ』も同様に登録してください。<br>
<img src="https://gitlab.com/kofu-senkouka/kicad/senkouka-lib/-/raw/doc/244255015-162c5fcf-601b-4646-b9fc-d208a42824d2.png" width="70%" /><br>
